var appointments = angular.module('AppointmentsApp')

appointments.controller('SearchController',
    ['$scope', '$filter', 'CustomerService',
    function($scope, $filter, CustomerService) {

      // Variable Declarations
      $scope.selected = {}
      $scope.customerSelected = false
      $scope.customers = CustomerService.AllCustomers
      $scope.state = "closed"

      $scope.change = function() {
        if ($scope.customerquery === "") {
          $scope.state = "closed"
          return
        }

        $scope.customers = $filter('customerSearchFilter')(CustomerService.AllCustomers, $scope.customerquery)
        if ($scope.customers.length > 0) {
          $scope.state = "opened"
        }
        else {
          $scope.state = "closed"
        }
      }

      $scope.selectCustomer = function(customer) {
        $scope.selected = customer
        $scope.customerSelected = true
        $scope.state = "closed"
      }

      $scope.clearSearch = function() {
        $scope.query = ""
        $scope.selected = ""
        $scope.customerSelected = false
      }
}])

appointments.filter('customerSearchFilter', function() {
  return function(collection, querystring) {
    var filteredArray = []

    querystring = querystring.toLowerCase()
    angular.forEach(collection, function(item) {
      if( item.forename.toLowerCase().indexOf(querystring) >= 0 ) filteredArray.push(item)
      else if( item.surname.toLowerCase().indexOf(querystring) >= 0 ) filteredArray.push(item)
    })

    return filteredArray
  }
})
