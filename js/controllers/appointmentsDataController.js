var appointments = angular.module('AppointmentsApp')

appointments.controller('AppointmentsDataController',
    ['$scope', 'AppointmentsService',
    function($scope, AppointmentsService) {

  $scope.visibleAppointments = []

  $scope.showToday = function() {
    $scope.visibleAppointments = AppointmentsService.GetAppointmentsByDates(0, 1)
  }

  $scope.showWeek = function() {
    $scope.visibleAppointments = AppointmentsService.GetAppointmentsByDates(3, 4)
  }

  $scope.showMonth = function() {
    $scope.visibleAppointments = AppointmentsService.GetAppointmentsByDates(14, 15)
  }
}])
