var appointments = angular.module('AppointmentsApp')

appointments.controller('AppointmentsCreationController',
    ['$scope', 'AppointmentsService',
    function($scope, AppointmentsService) {

    // Variable Declarations
    $scope.appointmentToCreate = {}
    $scope.selectedDate

    // Scope Functions
    $scope.attemptSave = function() {
      if (validateCreation()) {
        AppointmentsService.SaveAppointment($scope.appointmentToCreate)
      }
    }

    $scope.clearErrorMessage = function() {
      $scope.showErrorMessage = false
      $scope.errorMessage = ""
    }

    // Private Functions
    function validateCreation() {
      if (!$scope.appointmentToCreate.title || $scope.appointmentToCreate.title === "") {
        setErrorMessage("Error: Title cannot be empty.")
        return false
      }
      return true
    }

    function setErrorMessage(message) {
      $scope.showErrorMessage = true
      $scope.errorMessage = message
    }
}])
