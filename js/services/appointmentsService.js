var appointments = angular.module('AppointmentsApp')
var fs = require('fs')

appointments.service('AppointmentsService',
  ['$filter',
  function($filter) {

    // Get and Store all
    var appointmentsFromSource = fs.readFileSync('./data/appointments/appointments.json', 'utf8')
    if (appointmentsFromSource)
      this.AllAppointments = JSON.parse(appointmentsFromSource)
    else
      this.AllAppointments = []

    this.GetAppointmentsByDates = function(from, to) {
      var fromDate = new Date()
      fromDate.setHours(0,0,0,0)
      var toDate = new Date()
      toDate.setHours(0,0,0,0)

      return $filter('dateFilter')(this.AllAppointments,
        fromDate.setDate(fromDate.getDate() - from),
        toDate.setDate(toDate.getDate() + to))
    }

    this.SaveAppointment = function(appointment) {
      console.log("Adding appointment: " + JSON.stringify(appointment) + " to All appointments.")
      this.AllAppointments.push(appointment)
      //saveAppointmentsToFile(this.AllAppointments)
    }

  // this.getAppointmentById = function()
}])

function saveAppointmentsToFile(array) {
  var fs = require('fs')

  var toSave = JSON.stringify(array)

  fs.writeFile('./data/appointments/appointments.json', toSave, function(err) {
    if (err) {
      return console.log(err)
    }
  })
}

appointments.filter('dateFilter', function() {
  return function(collection, fromDate, toDate) {
    var filteredArray = []

    for (i=0; i<collection.length; i++) {
      apptDate = new Date(collection[i].startTime)

      if (apptDate > fromDate && apptDate < toDate) {
        filteredArray.push(collection[i])
      }
    }

    return filteredArray
  }
})
