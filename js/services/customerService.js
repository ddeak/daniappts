var appointments = angular.module('AppointmentsApp')
var fs = require('fs')

appointments.service('CustomerService',
  ['$filter',
  function($filter) {
    var customersFromSource = fs.readFileSync('./data/customers/customers.json', 'utf8')

    if (customersFromSource)
      this.AllCustomers = JSON.parse(customersFromSource)
    else
      this.AllCustomers = []

}])
