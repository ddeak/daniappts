var appointments = angular.module('AppointmentsApp', ['ngAnimate']);
var fs = require('fs')

// Load Jquery - needed as loading via script does not work.
window.$ = window.jQuery = require('./js/libs/jquery-2.1.4.min.js');

appointments.controller('AppointmentsCoreCtrl', ['$rootScope', '$scope', function($rootScope, $scope) {
  // Application Title
  $rootScope.header = "Appointments"

  // Views to display in main content window.
  $rootScope.currentView = "./views/Appointments/today.html"

  $scope.setView = function(view) {
    switch (view) {
      case 'Today':
        $rootScope.currentView = "./views/Appointments/today.html"
        break
      case 'Week':
        $rootScope.currentView = "./views/Appointments/thisWeek.html"
        break
      case 'Month':
        $rootScope.currentView = "./views/Appointments/thisMonth.html"
        break
      case 'NewAppointment':
        $rootScope.currentView = "./views/Appointments/new.html"
        break
      default:
        $rootScope.currentView = "./views/Appointments/today.html"
        break
    }
  }
}])
