var appointments = angular.module('AppointmentsApp')

appointments.directive('apptsdatepicker', function(){
  return {
    restrict: 'A',
    link: function (scope, element) {
      $(element).datepicker({
        format: 'd/m/yyyy',
        autoclose: true,
        todayHighlight: true,
        showTime: true,
        time24h: true
      });
    }
  }
});

appointments.directive('apptstimepicker', function(){
  return {
    restrict: 'EA',
    scope: false,
    link: function (scope, element) {
      $(element).timepicker({
        minTime: '8:00am',
        maxTime: '8:00pm',
        timeFormat: 'g:i A',
        showDuration: true,
        scrollDefaultNow: true
      })
      $(element).on( "change", function() {
        if (!scope.selectedDate)
          scope.selectedDate = new Date()

        scope.$apply(function() {       
          var time = element.val().match(/(\d+)(?::(\d\d))?\s*(p?)/)
          scope.selectedDate.setHours(parseInt(time[1]) + (time[3] ? 12 : 0) + 1)
          scope.selectedDate.setMinutes( parseInt(time[2]) || 0 )
        })
      });
    }
  }
});
