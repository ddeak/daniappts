var app = require('app')
var BrowserWindow = require('browser-window')

app.on('ready', function() {
  initApp()
})

function initApp() {
  var mainWindow = new BrowserWindow({
    width: 1000,
    height: 725
  })

  mainWindow.loadUrl('file://' + __dirname + '/index.html')
  mainWindow.setMenu(null)
}
